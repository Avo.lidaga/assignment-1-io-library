minus: db "-"

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    	cmp byte [rdi + rax] , 0        ; cmp расставляет флаги от чего если строка равна 0 (закончилась) выставиться ZF = 1
	je .end                         ; если ZF == 1 переходим в end
        inc rax                         ;  rax счетчик
        jmp .loop
        .end:
    	    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
        push rdi
	call string_length	; Для вывода строки нужно знать кол-во байт она состоит
        pop rdi
	mov rsi, rdi			; Так же для sys_write нужна сама строка
	mov rdx, rax
	mov rax, 1
        mov rdi, 1
	syscall
     ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi			; На вход идет код символа в ascii , а для вывода нужно указатель на него
	mov rsi,rsp 			; Адрес вершины стека и является указателем
	mov rax,1			; Все остальное как для обычного sys_write
	mov rdi,1
	mov rdx,1
	syscall
	pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1			
    mov rdi, 1 								
    mov rsi, 0xA
    mov rdx, 1								
    syscall
    ret

	

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rdx, rdx 			; rdx -  счётчик

    .loop:
        cmp byte [rdi + rdx], 0		; проверка 1 строки
        je .first_end
        
        mov cl, byte [rdi + rdx]
        cmp cl, byte [rsi + rdx]

        jne .unsuccessful

        inc rdx
        jmp .loop

    .first_end:				 ; Первая строка закончилась
        cmp byte [rsi + rdx], 0
        je .successful			 ; Вдруг вторая строка тоже закончилась
        jmp .unsuccessful

    .successful:
    	xor rax, rax
	inc rax	; Строки равны, возвращаем 1
        ret	

    .unsuccessful:
        xor rax, rax			; Строки не равны, возвращаем 0
        ret






; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax

    
	mov rax, 0			; sys_read также вызываем
	mov rdi,0

	push rax

	mov rsi, rsp			; Так как в rsp лежит адрес вершины стека,  то значит там и лежит тот символ который мы ввели
	mov rdx,1
	syscall
	pop rax


    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор



read_word:

	mov r8, rdi						; Регистры на начало
	mov r9, rsi						; размер
	mov r10, 0						; счетчик
.spaces:
	push r8
	push r9
	push r10
	call read_char
	pop r10
	pop r9
	pop r8
	cmp rax, 0
    je .end
    cmp rax, 0x20
    je .spaces
    cmp rax, 0x9
    je .spaces
    cmp rax, 0xA
    je .spaces

.common_r:
    cmp r9, r10
    je .err
    mov [r8 + r10], rax
    inc r10
	push r8
        push r9
        push r10
        call read_char
        pop r10
        pop r9
        pop r8
	cmp rax, 0
	je .end
	cmp rax, 0x20
    je .end
    cmp rax, 0x9
    je .end
    cmp rax, 0xA 						; Проверка на все пробельные символы
    je .end
    jmp .common_r

.err:
	xor rax, rax
	jmp .ret
.end:
	mov qword[r8+r10], 0
	mov rax, r8
	mov rdx, r10
.ret:
	
	ret



 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
        xor rax, rax
        xor rdx, rdx							;Подготовка регистров для хранение результата(число и длина)
        mov r9, 10							; 10 система счисления
        lea rdi, [rdi]
    .loop:
        cmp byte [rdi], '0' 						; Проверка на то что это число
        jb .end 							; Проверка и в случае перехода завершение функции
        cmp byte [rdi], '9'
        ja .end
        push rdx
        mul r9								 ; Перевод в 10 сс
        pop rdx
        add al, byte [rdi]
        sub al, '0'
        inc rdx								 ; Длина числа - счетчик
        inc rdi	
        lea rdi, [rdi]
        jmp .loop
    .end:
        ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
   
    cmp byte [rdi], '-' 					; проверка знаковое ли число
    jne .positive
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
.positive:
    jmp parse_uint						; Можно преобразовать как беззнаковое


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax ,rax


    	push rdi
.copy:
	mov rcx, 0
	mov cl, [rdi] 						; cl Применяю для того чтобы исправить ошибку при коипровании из памяти в память
	mov [rsi], cl
	inc rsi 
	inc rdi 						; Следующий символ
	test rcx,rcx 
	jnz .copy 						;ZF=0 -> Копирую
	pop rdi
	call string_length
	inc rax
	cmp rdx,rax
	jae .end 
	xor rax,rax 						; не помещается в буфер итог 0
.end:
	ret



; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
	mov rax , rdi                                           
	mov r10 , 10						;Регистр для хранения 10 системы счисления чтобы делить
	mov rdi , rsp 						
	sub rsp , 21						
	dec rdi 						; Здесь помещаем указатель для нового символа
	mov byte[rdi] , 0 
.loop:
	xor rdx , rdx						; Хранение остатка
	div r10							; Переводим число, частное - в rax, остаток - в rdx
	add rdx , '0' 						
	dec rdi
	mov [rdi] ,dl						;Кладем символ в стек
	test rax , rax 						;Здесь идет проверка на то что в аккумуляторе есть еще число
	jnz .loop 
.print:
	call print_string					
	add rsp , 21						
	ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	mov rax, rdi
	test rax, rax
	js .negative							; Если SF = 1 то есть отрицательное число
	call print_uint
    ret
	.negative:
		mov rbx, rdi
		mov rax, 1						; sys_write  обычный
		mov rdi, 1
		mov rsi, minus
		mov rdx, 1
		syscall

		mov rax, rbx
		neg rax
		mov rdi, rax
		call print_uint
	ret
	

